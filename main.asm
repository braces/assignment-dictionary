%include "words.inc"
%include "lib.inc"

extern find_word

%define BUFFER_SIZE 256

global _start
section .data

section .data
	not_found: db 'Key not found', 10, 0
	buffer_overflow: db 'Error. Can not read', 10, 0

section .text

_start:
	xor rax, rax
	mov rdi, BUFFER_SIZE
	mov rsi, BUFFER_SIZE
	call read_word
	test rax, rax
	je .overflowed
	mov rdi, rax
	mov rdi, w0
	push rdx 
	call find_word
	test rax, rax
	jne .found
	mov rdi, not_found
	call print_err
	jmp .err_end
.overflowed
	mov rdi,buffer_overflow
	call print_err
	jmp .err_end
.found:
	pop rdx
	add rax, 8
	add rax, rdx
	inc rax
	mov rdi, rax
	call print_string
	call print_newline
	jmp .end
.err_end:
    mov rdi, 1
.end:
    call exit




	
	
	
	
