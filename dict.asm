global find_word
extern string_length
extern string_equals
find_word:
	xor rax, rax	
.loop:
	add rsi, 8
	push rdi
	push rsi
	call string_equals
	pop rdi
	pop rsi
	cmp rax, 0
	jne .found_key 
	mov rsi, [rsi - 8]
	cmp rsi, 0
	je .no_key
	jmp .loop
.found_key:
	sub rsi, 8
	mov rax, rsi
	ret
.no_key:
	xor rax, rax
	ret

	
	

	
