section .text
 
global exit
global string_length
global print_string
global print_err
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

%define stdout 1
%define stderr 2


 
; Принимает код возврата и завершает текущий процесс
exit: 
    	mov rax, 60   
    	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину

string_length:
		xor rax, rax
.loop:
		cmp BYTE [rdi+rax], 0
		je .end
		inc rax
		jmp .loop
.end:
		ret
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov r10, stdout
    jmp print

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_err:
    mov r10, stderr
    jmp print

print:
	push rdi 
    call string_length
	pop rdi
    mov rsi, rdi
    mov rdx, rax
    mov rax, 1
    mov rdi, r10
    syscall
    ret



; Принимает код символа и выводит его в stdout
print_char:
	push rdi; 		;push rdi onto stack
	mov rsi, rsp
	mov rdx, 1		;1 byte 
	mov rdi, 1		;stdout
	mov rax, 1		;sys_write call
	syscall
	pop rdi			
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0xA
    jmp print_char
	

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	mov r10, 10		;divider
	push 0			;put 0 terminator to stack
.divloop:
	xor rdx, rdx
	div r10			;rax divide by 10,the remainder goes to rdx
	add rdx, 0x30	;to ASCII symbol
	push rdx		;save tto stack
	cmp rax, 0		;check number
	jne .divloop	
.getdigit:
	pop rdi			;get the next character
	cmp rdi, 0		
	je .end			;if = 0 end 
	push rdi
	call print_char	;print
	pop rdi
	jmp .getdigit	
.end:
	ret
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi	;check the sign
	jns .unsigned 	;jump  if unsigned number
	neg rdi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi 
.unsigned:
	push rdi
	call print_uint
	pop rdi
	ret
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    	xor rax, rax
.loop:
	mov r10b, byte [rdi + rax]
	mov r11b, byte [rsi + rax]
	inc rax
	cmp r10b, r11b
	jne .noteq
	test r10b, r10b;
	jne .loop
	mov rax, 1 
	ret
.noteq:
	mov rax, 0 
	ret
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	mov rdi, 0	;sys_read
	mov rdx, 1	;read 1 character
        push 0		;save 0 to stack 
	mov rsi, rsp    ;use stack as buffer
	syscall
	pop rax		;get the result from stack
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
   	xor rcx, rcx    
    	xor rax, rax    
.loop:
    	cmp rcx, rsi;compare nummber of char with buffer size
    	jae .ofl   
    	push rdx    ; caller-saved
    	push rsi    ; caller-saved
    	push rdi    ; caller-saved
    	push rcx    ; caller-saved
    	call read_char  
    	pop rcx     ; caller-saved
    	pop rdi     ; caller-saved
    	pop rsi     ; caller-saved
    	pop rdx     ; caller-saved
    	cmp al, 0xA ; check line break
    	je .handle   
    	cmp al, 0x9 ; check tab
    	je .handle 
    	cmp al, 0x20; check space
    	je .handle   
    	cmp al, byte 0  
    	je .end         
    	mov [rdi + rcx], al ; symbol to buffer
    	inc rcx     ; inc word length
    	jmp .loop       
.ofl:
    	xor rax, rax   
    	ret           
.handle
    	cmp rcx, 0 ; compare with 0   
    	je .loop       
.end:
    	mov [rdi + rcx], byte 0     
    	mov rax, rdi ; adress buffer to rax
    	mov rdx, rcx ; word length to rdx
    	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax 
	mov r9, 10
	xor r10, r10
.loop:
	xor r8, r8
	mov r8b, byte [rdi + r10]
	cmp r8b, 0x30
	jb .exit
	cmp r8b, 0x39
	ja .exit
	sub r8b, 0x30
	mul r9
	add rax, r8
	inc r10
	jmp .loop
.exit:
	mov rdx, r10
	ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
   	xor rax, rax
	xor rdx, rdx
	cmp byte[rdi] , '-'
	jne parse_uint
	inc rdi
	push rdi
	call parse_uint
	pop rdi
	inc rdx
	neg rax
	ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	push rdi
	call string_length
	pop rdi
	cmp rdx, rax
	jl	.notfit
	xor rax, rax
	jmp .loop
.loop:
	xor r9, r9
	mov r9b, [rdi + rax]
	mov [rsi +rax], r9b
	cmp r9, 0
	je .end
	inc rax
	jmp .loop
.notfit:
	xor rax, rax
	ret
.end: 
	ret
